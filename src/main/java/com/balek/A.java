package com.balek;

import com.balek.ne.faites.pas.des.trucs.comme.ça.moi.je.peux.me.mettre.à.pleurer.vous.savez.UtilInfecte;
import org.osef.B;

public class A {
    final B b;

    public A(final B b) {
        this.b = b;
        final var overridableProp = UtilInfecte.getOverridableProp();
        System.out.println("overridableProp (in A) = " + overridableProp);
        final var main = UtilInfecte.getProp("main");
        System.out.println("main = " + main);
    }

    public String mahChildName() {
        return b.toString();
    }
}
