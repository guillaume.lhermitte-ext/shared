package org.osef;

import com.balek.A;
import com.balek.ne.faites.pas.des.trucs.comme.ça.moi.je.peux.me.mettre.à.pleurer.vous.savez.UtilInfecte;

public class B {
    final A a;

    public B(final A a) {
        this.a = a;
        final var overridableProp = UtilInfecte.getOverridableProp();
        System.out.println("overridableProp (in B) = " + overridableProp);
        final var pied = UtilInfecte.getProp("pied");
        System.out.println("pied = " + pied);
    }

    public String sayYourNaymeSayYourNamr() {
        return a.toString();
    }
}
