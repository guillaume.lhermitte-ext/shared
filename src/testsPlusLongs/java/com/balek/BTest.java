package com.balek;

import com.balek.ne.faites.pas.des.trucs.comme.ça.moi.je.peux.me.mettre.à.pleurer.vous.savez.UtilInfecte;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.osef.B;

import static org.assertj.core.api.Assertions.assertThat;
import static org.osef.util.en.tous.cas.d.après.le.nom.UtilDégueu.méthodePasOuf;
import static org.osef.util.en.tous.cas.d.après.le.nom.UtilDégueu.okValue;

class BTest {

    @BeforeAll
    static void b4(){
        final var overridableProp = UtilInfecte.getOverridableProp();
        System.out.println("overridableProp = " + overridableProp);
        final var testsPlusLong = UtilInfecte.getProp("testsPlusLong");
        System.out.println("testsPlusLong = " + testsPlusLong);
    }
    /** Ce test est dégueulasse, les valeurs cachées dans les utilitaires de mocking ...
     * C'est pas comme ça qu'on fait les choses ...
     * Bon il est court c'est déjà ça ... */
    @Test
    void weShouldAccessAllSrcAndOtherTstSrc() {
        final var sut = new B(méthodePasOuf(A.class));
        assertThat(sut.sayYourNaymeSayYourNamr()).isEqualTo(okValue());
    }
}
