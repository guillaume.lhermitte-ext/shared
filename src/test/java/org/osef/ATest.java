package org.osef;

import com.balek.A;
import com.balek.ne.faites.pas.des.trucs.comme.ça.moi.je.peux.me.mettre.à.pleurer.vous.savez.UtilInfecte;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.osef.util.en.tous.cas.d.après.le.nom.UtilDégueu.méthodePasOuf;
import static org.osef.util.en.tous.cas.d.après.le.nom.UtilDégueu.okValue;

class ATest {

    @BeforeAll
    static void b4(){
        final var overridableProp = UtilInfecte.getOverridableProp();
        System.out.println("overridableProp = " + overridableProp);
        final var test = UtilInfecte.getProp("test");
        System.out.println("test = " + test);
    }
    /** Ce test est encore plus dégueulasse que {@link com.balek.BTest}, pour les même raisons */
    @Test
    void weShouldAccessAllSrcAndOtherTstSrc() {
        final var sut = new A(méthodePasOuf(B.class));
        assertThat(sut.mahChildName()).isEqualTo(okValue());
    }

}
