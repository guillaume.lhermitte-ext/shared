package org.osef.util.en.tous.cas.d.après.le.nom;

import com.balek.A;
import org.osef.B;

public class UtilDégueu {
    /**
     * dE QUI S4MOCK-T-ON !!! 🤣🤣🤣 (cette méthode est turbo unsafe et giga crado ...)
     */
    public static <R> R méthodePasOuf(final Class<R> rClass) {
        if (rClass.getSimpleName().equals("B")) {
            return (R) new B(null) {
                public String toString() {
                    return okValue();
                }
            };
        }
        return (R) new A(null) {
            public String toString() {
                return okValue();
            }
        };
    }
    public static String okValue() {
        return "testOk";
    }
}
